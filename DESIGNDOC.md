## Design Doc 

+ Add nav comp dependency in __build.gradle__
+ Enable Data Binding in __build.gradle__
+ Add __navigation.xml__
+ Enable __Safe__ __Args__ plugin to send data from one screen to another _(AT SECOND THOUGHT THIS IS NOT NECESSARY IN THE FIRST VERSION OF THE APP)_
+ Set up 2 Fragments
+ Transform those 2 Fragments into Data Binding layouts through setting `<layout>` as the enclosing element, also in here will be the reference to the viewmodel via a __variable__ tag
This is useful for binding the variables from the viewmodel to the layout and also for setting up the click handlers via listener bindings
+ The first Fragment will be visible only on the first visit and has an __EditText__ field & a __Button__ -> Use Data Binding & ViewModel 
+ Save input to __SharedPreferences__ after button tap: 
That button should use Data Binding and sth similar to: `() -> viewmodel.saveToSharedPreferences()` (setting the event handler in the XML is also known as “Listener Bindings for event handling” -> should be set on the __onClick__ property of the Button in the XML
This method should be implemented in the viewmodel.
+ There should be another button that should move you to the 2nd screen -> this could be a Dashboard or similar:
On first thought I wanted to transfer the entered name from the first screen to the second screen via __Safe__ __Args__ but as the 2nd screen should always be the first screen if the user is not a 'first time user' of the app the data should be retrieved from __SharedPreferences__ -> should be done in __onCreateView__ 
The __activity_main.xml__ should include a __<fragment>__ tag as its only child element,this should serve the purpose of hosting any fragment that needs to be displayed
+ At this point the 2 fragments can be added to the navigation graph via the nav graph and connected with an action to indicate a possible route
+ The fragments need to connect the Binding objects with the viewmodels and set themselves as __lifecycleowner__ s to be able to observe a Button tap and do the navigation:
The navigation can't be done in the viewmodel. 
Although the viewmodel has the function that was referenced in the __onClick__ inside the XML it can't reference any View related object -> Navigation needs a reference to the fragment instance which is not available in the viewmodel.
Current approach: Change the boolean value of a variable in the viewmodel when the button is tapped and observe that variable change in the Fragment.
An easier way may be to just use a __setOnClickListener__ lambda.


